F-Droid is an installable catalogue of FOSS (Free and Open Source Software)
applications for the Android platform. The client makes it easy to browse,
install, and keep track of updates on your device.

The F-Droid server tools provide various scripts and tools that are used to
maintain the main F-Droid application repository. You can use these same tools
to create your own additional or alternative repository for publishing, or to
assist in creating, testing and submitting metadata to the main repository.

For documentation, please see the docs directory.

Alternatively, visit [https://f-droid.org/manual/](https://f-droid.org/manual/)


Installing
----------

The easiest way to install the `fdroidserver` tools is on Debian, Ubuntu, Mint
or other Debian based distributions, you can install using:

    sudo add-apt-repository ppa:guardianproject/ppa
    sudo apt-get update
    sudo apt-get install fdroidserver

But you can also use `virtualenv` and `pip` python tools that also work on other
distributions.

First, make sure you have installed the python header files, virtualenv and pip.
They should be included in your OS's default package manager or you can install
them via other mechanisms like Brew/dnf/pacman/emerge/Fink/MacPorts.

For Debian based distributions:

    apt-get install python-dev python-pip python-virtualenv

Then here's how to install:

    git clone https://gitlab.com/fdroid/fdroidserver.git
    cd fdroidserver
    virtualenv env/
    source env/bin/activate
    pip install -e .
    python2 setup.py install
